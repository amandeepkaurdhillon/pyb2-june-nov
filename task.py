a = '''
    Press 1: To Add Elements
    Press 2: To View Elements
    Press 3: To sum
    Press 4: To remove elements
    Press 5: To exit
'''
print(a)
ls = []
while True:
    choice = input("Enter Your Choice ")
    if choice == "1":
        elem = int(input("Enter Element to Add "))
        ls.append(elem)
        print("Element {} added Successfully!!!".format(elem))
    elif choice=="2":
        c=1
        for i in ls:
            print(c,' : ',i)
            c+=1
    elif choice == "3":
        s=0 
        for j in ls:
            s+=j
        print("Sum of {} elements is {}".format(len(ls),s))
    elif choice == "4":
        rm = int(input("Enter Element to Remove "))
        ls.remove(rm)
        print("Element {} removed Successfully!!!".format(rm))
    elif choice == "5":
        break
    else:
        print("Wrong Input")

        
